FROM ubuntu:20.04

RUN echo Europe/London > /etc/timezone
RUN echo tzdata tzdata/Areas select Europe | debconf-set-selections
RUN echo tzdata tzdata/Zones/Europe select London | debconf-set-selections

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
        build-essential \
        cmake \
        libv4l-dev \
        qtbase5-dev \
        qtbase5-dev-tools \
        && apt-get clean

RUN groupadd -g 1000 dockerbot && useradd -g 1000 -u 1000 -m dockerbot
USER dockerbot
WORKDIR /build